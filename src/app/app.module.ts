import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { environment } from './../environments/environment';
import { FireUsersComponent } from './fire-users/fire-users.component';
import { NavigationComponent } from "./navigation/navigation.component";
import { RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { HttpModule } from '@angular/http';
import { ProductsComponent } from './products/products.component';
 import { NotFoundComponent } from './not-found/not-found.component';
 import { UsersService } from "./users.service";
import { LoginComponent } from './login/login.component';
import { ProductsSearchComponent } from './products/products-search/products-search.component';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';


@NgModule({
  declarations: [
    AppComponent,
    FireUsersComponent,
    NavigationComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    LoginComponent,
    ProductsSearchComponent,
    EditProductComponent,

  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path:'', component:ProductsComponent}, //default - localhost:4200 - homepage
      {path:'products', component:ProductsComponent}, //localhost:4200/users
      {path:'login', component:LoginComponent}, //localhost:4200/users
      {path: 'edit-product/:id', component: EditProductComponent},
      {path:'fireusers', component:FireUsersComponent},
      {pathMatch:'full', path:'users-firebase', component:FireUsersComponent},
      {path:'**', component:NotFoundComponent} //all the routs that donwt exist

    ])

  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
