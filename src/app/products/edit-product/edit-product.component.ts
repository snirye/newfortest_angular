import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import {FormGroup , FormControl,FormBuilder} from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { UsersService } from "../../users.service";

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
   
  @Output() updateUser:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() updateUserPs:EventEmitter<any> = new EventEmitter<any>();

  service:UsersService;
  user;

  name;
  price;

  updateform = new FormGroup({
    name:new FormControl(),
    price:new FormControl()
  });

  /*
  constructor(private route: ActivatedRoute ,service: UsersService) {
    this.service = service;
  }

  */

    constructor(private route: ActivatedRoute ,service: UsersService, private formBuilder: FormBuilder, private router: Router) {
    this.service = service;
  }


  sendData() {
    this.updateUser.emit(this.updateform.value.name);
    console.log(this.updateform.value);

    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putUser(this.updateform.value, id).subscribe(
        response => {
          console.log(response.json());
          this.updateUserPs.emit();

          this.router.navigate(['/']);
        }
      );
    })
  }
 //הצגת מספר מזהה בשביל הטופס
  //user;
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user);

        this.name = this.user.name
        this.price = this.user.price  

        
      })
    })
  }
}
