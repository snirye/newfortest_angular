import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {


  users;
  usersKeys;

  constructor(private service:UsersService) {
    service.getUsers().subscribe(response=>{
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
    });
   }

  ngOnInit() {
  }

}