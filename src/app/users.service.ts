import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
 import {environment} from './../environments/environment';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';


@Injectable()
export class UsersService {

  //http:Http;

  getUsers(){
    //get users from the SLIM rest API (Don't say DB)
    //return  this.http.get('http://localhost/angular/Test/Slim/users');
      return  this.http.get(environment.url + 'users');                                     //*** build ****/
  }

  getUsersFire(){
    return this.db.list('/users').valueChanges();    
  }

  //Get one user
 getUser(id){
  //return this.http.get('http://localhost/angular/Test/Slim/users/'+ id);
   return this.http.get(environment.url + 'users/'+id);                                       //*** build ****/
  }

  //Put
  putUser(data,key){
 let options = {
   headers: new Headers({
     'content-type':'application/x-www-form-urlencoded'
   })
  }
 //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
 let params = new HttpParams().append('name',data.name).append('price',data.price);
 //return this.http.put('http://localhost/angular/Test/Slim/users/'+ key,params.toString(), options);
  return this.http.put(environment.url + 'users/'+ key,params.toString(), options);                  //*** build ****/
  }

  constructor(private http:Http, private db:AngularFireDatabase) { 
    
  }

}

